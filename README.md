# Weather App
  - REST API using Rails 5
  - Frontend using React.js
  - Services using Docker for scalability

## Getting Started
### Using Docker
Just run:
```sh
$ docker-compose up
```
### Without Docker
- Run rails server:
```sh
$ cd backend
$ bundle install
$ bundle exec rails db:migrate
$ bundle exec rails db:seed
$ bundle exec rails s -p 3001 -b '0.0.0.0'
```
- Run the frontend:
```sh
$ cd fronend
$ npm install
$ npm start
```

## Architecture
A web app running as a service. Sending AJAX requests to the RESET API. The API service requests latest weather info from openweathermap.org and caches the info in sqlite database.
