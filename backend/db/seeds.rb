# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require 'json'

# Seed countries
file = File.read('/myapp/db/countries.json')
data_hash = JSON.parse(file)
values = data_hash.map { |c| "('#{c['name'].gsub("'", "\'")}','#{c['code']}',date('now'),date('now'))" }.join(",")
ActiveRecord::Base.connection.execute("INSERT INTO #{Country.table_name} (name, code, created_at, updated_at) VALUES #{values}")

puts 'Done with countries. Now Cities. This will take some time.'

# Seed cities
file = File.read('/myapp/db/city.list.json')
data_hash = JSON.parse(file)
values = data_hash.map { |c| "(#{c['id']},'#{c['country']}',\"#{c['name'].gsub('"', %q(\\\"))}\",#{c['coord']['lon']},#{c['coord']['lat']},date('now'),date('now'))" }
values.each_slice(500) { |slice|
  ActiveRecord::Base.connection.execute("INSERT INTO #{City.table_name} (api_id, country_code, name, lon, lat, created_at, updated_at) VALUES #{slice.join(",")}")
}
