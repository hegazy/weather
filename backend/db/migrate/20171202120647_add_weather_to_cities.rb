class AddWeatherToCities < ActiveRecord::Migration[5.0]
  def change
    add_column :cities, :weather, :string
  end
end
