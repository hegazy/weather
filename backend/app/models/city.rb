require 'net/http'

class City < ApplicationRecord
  belongs_to :country, primary_key: 'code', foreign_key: 'country_code'

  def as_json(options = nil)
    super({ include: [:country] }.merge(options || {}))
  end

  def weather
    weather = read_attribute(:weather)
    if weather == nil
      update_weather
    else
      JSON.parse weather
    end
  end

  def update_weather
    url = URI.parse("http://api.openweathermap.org/data/2.5/weather?id=#{self.api_id}&units=metric&appid=9395f68d3e94d729c4d8cc8ad6866afb")
    req = Net::HTTP::Get.new(url.to_s)
    res = Net::HTTP.start(url.host, url.port) {|http|
      http.request(req)
    }
    json = JSON.parse res.body
    update_attribute(:weather, json.to_json)
    return json
  end
end
