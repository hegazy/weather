Rails.application.routes.draw do
  resources :cities do
    collection do
      get 'random'
    end
  end
  resources :countries
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
