import React, { Component } from "react";
import axios from "axios";
import "./App.css";

class App extends Component {
  state = {
    city: null,
    countriesList: [],
    selectedCountry: null,
    citiesList: []
  };

  componentWillMount() {
    axios.get("http://localhost:3001/cities/random").then(({ data }) => {
      this.setState({ city: data });
    });
  }

  searchCountry = () => {
    clearTimeout(this.searchingCountry);
    this.searchingCountry = setTimeout(() => {
      const country = this.refs.country.value;
      axios.get("http://localhost:3001/countries?q=" + country).then(({ data }) => {
        this.setState({ countriesList: data });
      });
    }, 300);
  };

  searchCity = () => {
    clearTimeout(this.searchingCity);
    this.searchingCity = setTimeout(() => {
      const city = this.refs.city.value;
      const { selectedCountry } = this.state;
      if (selectedCountry) {
        axios.get("http://localhost:3001/cities?q=" + city + "&country=" + selectedCountry.code).then(({ data }) => {
          this.setState({ citiesList: data });
        });
      }
    }, 300);
  };

  selectCountry = selectedCountry => {
    this.setState({ selectedCountry, countriesList: [] });
    this.refs.country.value = selectedCountry.name;
  };

  selectCity = city => {
    this.setState({ city, citiesList: [] });
    this.refs.city.value = city.name;
  };

  render() {
    const { city, countriesList, citiesList } = this.state;
    return (
      <div className="App">
        <div className="search">
          <label>Search: </label>
          <span className="search__item">
            <input ref="country" type="text" placeholder="Country" onChange={this.searchCountry} />
            {countriesList.length > 0 && (
              <ul className="search__dropdown">
                {countriesList.map(country => (
                  <li key={country.id} onClick={() => this.selectCountry(country)}>
                    {country.name}
                  </li>
                ))}
              </ul>
            )}
          </span>
          <span className="search__item">
            <input ref="city" type="text" placeholder="City" onChange={this.searchCity} />
            {citiesList.length > 0 && (
              <ul className="search__dropdown">
                {citiesList.map(city => (
                  <li key={city.id} onClick={() => this.selectCity(city)}>
                    {city.name}
                  </li>
                ))}
              </ul>
            )}
          </span>
        </div>

        {city && (
          <div className="weather">
            <div className="weather__value">{city.weather.main.temp}</div>
            <div className="weather__city">
              {city.name}, {city.country.name}
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default App;
